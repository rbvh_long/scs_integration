<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/kaccwishlist/desktop/account" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/desktop/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="bazaarvoice" tagdir="/WEB-INF/tags/addons/kaccbazaarvoice/desktop/bazaarvoice"%>

<body>
	<div class="my-account-wishlist">
		
		<div class="headline">
			<spring:theme code="kacc.wishlist.account.myWishlist"/>
		</div>
		<p class="intro">
			<strong> <spring:theme code="kacc.wishlist.account.shortText"/></strong><br> 
			<span> <spring:theme code="kacc.wishlist.account.longText"/> </span>
		</p>
		
		<form action="#" class="addWishList">
		<c:set var="count" value="0"/>
				<c:choose>
					<c:when test="${not empty  wishlistDatas}">
						<select id="wishcombo" name="wishcombo" onchange="performChooseWishlistValue()">
							<c:choose>
								<c:when test="${isSelected eq true}">
									<c:forEach var="item" items="${wishlistDatas}">
										<c:choose>
											<c:when test="${selectedWishList eq item.name}">
												<option selected="${selectedWishList}">${selectedWishList}</option>
											</c:when>
											<c:otherwise>
												<option value="${item.name}">${item.name}</option>
											</c:otherwise>
										</c:choose>
										<c:set var="count" value="${count + 1}"/>
									</c:forEach>	
								</c:when>
								<c:otherwise>
									 <option selected="selected" disabled="disabled" style="display:none;">
						 				<spring:theme code="kacc.wishlist.default.option.message" />
						 				<c:forEach var="item" items="${wishlistDatas}">
											<option value="${item.name}">${item.name}</option>
										</c:forEach>
						 			</option>
								</c:otherwise>
							</c:choose>
							
						</select>
					</c:when>
					<c:otherwise>
						<p><spring:theme code="kacc.wishlist.empty.wishlist.message" /></p>
					</c:otherwise>
				</c:choose>
				
				<c:choose>
				<c:when test="${count >=10}">
				<a class="linkToPopinDisable addWishlistPopinLink right varchar">
					<spring:theme code="kacc.wishlist.account.addWishlist"/>
					</a>  
				</c:when>
				<c:otherwise>
				<a href="${pageContext.request.contextPath}/wishlist/addwishlist" class="linkToPopin addWishlistPopinLink right">
					<spring:theme code="kacc.wishlist.account.addWishlist"/>  
					
			   </a>
			 
			   </c:otherwise>
			   </c:choose>
		</form>
		
		<c:if test="${isSelected eq true}">
			<div id="whishConfirmationMssg" > 
				<p><span> <spring:theme code="kacc.wishlist.account.deleteWishlist"/>  ${wishlistname} ?</span>
					<button class="cancelDelete" onclick="cancelDelete();" ><spring:theme code="wishlist.remove.popup.cancelbutton"/></button>
					<button class="confirmDelete" onclick="confirmDelete();"><spring:theme code="wishlist.remove.popup.submit"/></button>
				</p>
			</div>
			<div id="whishConfirmationBtn" >
				<button onclick="showConfirmationMssg();" > <spring:theme code="wishlist.remove.popup.submit"/></button>
			</div>
		</c:if>
		<div id="idResultRemove">
		
		</div>
		
		
		<div class="wishlist">
			<span class="title-envie">${selectedWishList}</span>
		<c:choose>
			<c:when test="${not empty wishlistEntries}">
			
			<c:set var="selectedWishList" value="${selectedWishList}"/>
			<c:set var="selectedWishList2" value="${fn:replace(selectedWishList,' ','%20')}"/>
			<c:set var="urlResources"  value="${contextPath}/_ui/addons/kaccwishlist/${fn:toLowerCase(uiExperienceLevel)}/common"/>
			<span class="share">
					<input type="hidden" id="iurl" value="${pageContext.request.contextPath}/my-account/wishlist/share?wishlitCode=${selectedWishList2}" />
					<a href="#"onclick="sendEmail()"  class="rdc-popin">
						<spring:theme code="kacc.wishlist.account.wishlistShare" />  
						<img src="${urlResources}/images/share-mail.png"	alt="Envoyer à un ami" />
					</a>
			</span>
			<span class="shareSocialMediaStyle">
				<!-- AddToAny BEGIN -->
				<a class="a2a_dd" href="https://www.addtoany.com/share_save">
					<spring:theme code="kacc.wishlist.account.share.socialMedia" />  
					<img src="${urlResources}/images/socialShareIcon.png"  />
				</a>
					<script type="text/javascript">
						var a2a_config = a2a_config || {};
						a2a_config.locale = "fr";
					</script>
				<script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
				<!-- AddToAny END -->
			</span>
			
			
			
			<div class="list-content">
				<c:forEach var="entry" items="${wishlistEntries}" varStatus="status">
					<c:if test="${status.count % 2 == 1}">
						<c:set var="even" value="" />
						<c:set var="stock" value="indisponible" />
						<c:set var="classStock" value="indisponible" />
						<c:set var="btnpanier" value="disable" />
					</c:if>
					<c:if test="${status.count % 2 != 1}">
						<c:set var="even" value="even" />
						<c:set var="stock" value="En stock" />
						<c:set var="classStock" value="en-stock" />
						<c:set var="btnpanier" value="" />
					</c:if>
					<div class="item-row ${even}">
						<div class="img">
							<a href="#" title="Nom du produit"> <c:choose>
									<c:when test="${not empty entry.product.images}">
										<c:set var="image" value="${entry.product.images[0]}" />
										<c:choose>
											<c:when test="${not empty image.altText}">
												<img src="${image.url}" alt="${fn:escapeXml(image.altText)}" title="${fn:escapeXml(image.altText)}" />
											</c:when>
											<c:otherwise>
												<img src="${image.url}"
													alt="${fn:escapeXml(entry.product.name)}"
													title="${fn:escapeXml(entry.product.name)}" />
											</c:otherwise>
										</c:choose>
									</c:when>
								</c:choose>
							</a>
						</div>
						<div class="context">
							<div class="pad">
								<div class="top">
									<div class="info">
										<div class="pad">
											<span class="date"> <spring:theme code="kacc.wishlist.account.add.date"/> ${entry.addedDateFormatted }</span> 
											<span class="description">
												<a title="Nom du produit" href="${pageContext.request.contextPath}/p/${entry.product.code}"> ${entry.product.name }</a>
											</span>											
										</div>
									</div>
									<div class="price">
											<format:fromPrice priceData="${entry.product.price}" />
									</div>
									<c:choose>
										<c:when test="${entry.product.stock.stockLevelStatus.code eq 'inStock'}">
											<div class="disponibilty disponible"> 
													<spring:theme code=""/> <spring:theme code="kacc.wishlist.account.product.instock"/>
											</div>
										</c:when>
										<c:otherwise>
											<div class="disponibilty indisponible">
												<spring:theme code="kacc.wishlist.account.product.outstock"/>
											</div>	
										</c:otherwise>
									</c:choose>	
									<div class="action">
											<addonProduct:productAddToCartButton product="${entry.product}"/>
									</div>
								</div>
								<div class="wishlistEntryRatingClass">
<!-- 									<span >  -->
<%-- 										<c:set var="averageRating" value="${fn:substringBefore(entry.product.averageRating, '.')}"></c:set> --%>
<%-- 										<c:forEach var="i" begin="1" end="${averageRating}"> --%>
<%-- 											<img src="${urlResources}/images/star-or-produit.png" alt="" /> --%>
<%-- 										</c:forEach>  --%>
<%-- 										<c:forEach var="i" begin="1" end="${5- averageRating}"> --%>
<%-- 											<img src="${urlResources}/images/star-gris-produit.png" alt="" /> --%>
<%-- 										</c:forEach> --%>
<!-- 									</span> -->
<%-- 									 ${entry.product.numberOfReviews} &nbsp; <spring:theme code="kacc.wishlist.account.review.numberOfreviews" /> --%>
								<bazaarvoice:inlineRatings product="${entry.product}" />
								</div>
								<div class="remove">
									<input type="hidden" id="idurl" value="${pageContext.request.contextPath}/wishlist/askremove/?productCode=${entry.product.code}&wishlistName=${fn:replace(selectedWishListData.name,' ','%20')}" />
									<a href="#"
										 onclick="confirmRemove()" > <spring:theme code="kacc.wishlist.account.remove.wishlistEntry"/> 
									</a>
								</div>
							</div>
						</div>
						<div></div>
					</div>
				</c:forEach>
				<div class="pager">
					<script type="text/javascript">
						var enablePaginate = true;
						var paginationItemsClass = '.list-content';
						var itemsPerPage = 10;
					</script>
				 </div>
			</div>
			</c:when>
			<c:otherwise>
				<h2><spring:theme code="kacc.wishlist.account.empty.entries.message"/></h2>
			</c:otherwise>
			</c:choose>
		</div>
	</div>


</body>
</html>