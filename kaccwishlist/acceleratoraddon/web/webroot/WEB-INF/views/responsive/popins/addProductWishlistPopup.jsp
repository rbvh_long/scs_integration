<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<input type="hidden" id="productCode" value="${productCode}" />
<input type="hidden" id="listFromCart" value="${listFromCart}" />
<c:choose>
	<c:when test="${isConnected}">
		<div id="idResult"></div>
		<div class="wishListPopin">

			<div class="select">
				<label for="idSelectOption"><spring:theme code="kacc.wishlist.add.wishlist.choice"/></label>
				<select id="idSelectOption">
					<option value="newWishlist" selected="selected">
						<spring:theme code="kacc.wishlist.new.option.message" />
					</option>
					<c:forEach items="${wishlists}" var="wishlist" varStatus="status">
						<option class="classOption" value="${wishlist.name}" ${status.first ? 'selected="selected"' : ''}>
							${wishlist.name}
						</option>
					</c:forEach>
				</select>
			</div>

			<c:if test="${fn:length(wishlists) lt 10}">
				<div id="newWishlistSection" class="hidden">
					<div class="text">
						<label for="idNewWish"><spring:theme code="kacc.wishlist.add.wishlist.message" /></label>
						<input id="idNewWish" name="idNewWish" type="text" class="required show"
								placeholder="<spring:theme code="kacc.wishlist.add.wishlist.name.placeholder" />" />
					</div>
				</div>

				<div class="actions">
					<a href="javascript:void();" id="wishlistCancel" class="button button--secondary">
						<spring:theme code="kacc.wishlist.add.wishlist.cancel" />
					</a>
					<spring:theme code="kacc.wishlist.add.wishlist.add" var="wishlistAddButton" />
					<spring:theme code="kacc.wishlist.add.wishlist.create" var="wishlistCreateButton" />
					<button id="idSubmitNewWish" class="button button--secondary"
							data-add-label="${wishlistAddButton}" data-create-label="${wishlistCreateButton}">
						${wishlistAddButton}
					</button>
				</div>

				<div class="row">
					<p class="alert alert-danger errorValidation wishlistError"><spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/></p>
					<p class="alert alert-danger errorSize wishlistError"><spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/></p>
				</div>
			</c:if>
		</div>
	</c:when>
	<c:otherwise>
		<div class="wishListPopin">
			<h1><spring:theme code="kacc.wishlist.login.wishlist.message" /></h1>
			<p>
				<spring:theme code="kacc.wishlist.login.wishlist.message.partOne" />
				<a id="idLoginhref" href="${pageContext.request.contextPath}/login">
					<spring:theme code="kacc.wishlist.login.wishlist.link" />
				</a>
				<spring:theme code="kacc.wishlist.login.wishlist.message.partTwo" />
			</p>
		</div>
	</c:otherwise>
</c:choose>
