package com.keyrus.accelerator.kaccwishlist.utils;

import java.util.ArrayList;
import java.util.List;

import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


public class WishListFormatterUtils
{
	private WishListFormatterUtils()
	{
		//empty
	}

	public static String getFormattedWhishList(final WishlistData wishData)
	{
		final List<WishlistEntryData> wishlistEntries = wishData.getEntries();

		final List<WishlistEntryData> allEntries = new ArrayList<>();

		final double totalPrice = 0;

		for (final WishlistEntryData entryData : wishlistEntries)
		{
			allEntries.add(entryData);
		}

		final StringBuilder printableList = new StringBuilder();
		printableList.append(
				"<table width='90%' cellspacing='10px' style='margin:10px;padding:10px;table-layout:auto;border:1px solid #000000;'>");
		// Wish list name
		printableList.append("<tr>");
		printableList.append("<td align='center' valign='middle' colspan=2><strong>");
		printableList.append(wishData.getName());
		printableList.append("</strong></td>");
		printableList.append("</tr><tr/>");

		printableList.append("<tr/><tr><td/><td><strong>GRAND TOTAL : " + totalPrice + "</strong></td></tr>");
		printableList.append("</table>");

		return printableList.toString();
	}
}
