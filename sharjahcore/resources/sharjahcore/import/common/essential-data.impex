# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# Import essential data for the Accelerator
#
# Includes:
# * Languages
# * Currencies
# * Titles
# * Vendors
# * Warehouses
# * Supported Credit/Debit cards
# * User Groups
# * DistanceUnits for Storelocator
# * MediaFolders
# * MediaFormats                               s
# * Tax & Tax Groups
# * Jobs
#% impex.setLocale(Locale.GERMAN);

# Languages
INSERT_UPDATE Language;isocode[unique=true];fallbackLanguages(isocode);active[default=true]
;en;;;
;ar;en;;

# Currencies
INSERT_UPDATE Currency;isocode[unique=true];conversion;digits;symbol
;AED;1;2;AED

# Vendor
INSERT_UPDATE Vendor;code[unique=true];name
;default;Default Vendor

# Disable preview for email pages
UPDATE CMSPageType;code[unique=true];previewDisabled
;EmailPage;true

# Titles
INSERT_UPDATE Title;code[unique=true]
;mr
;mrs
;miss
;ms
;dr
;rev

INSERT_UPDATE gender; code [unique = true]; name[lang = ar]; name[lang = en]
;	FEMALE ; أنثى; FEMALE
;	MALE   ; ذكر; MALE

INSERT_UPDATE MaritalStatus; code [unique = true]; name[lang = ar]; name[lang = en]
;	SINGLE 		; غير متزوج; SINGLE
;	MARRIED   	; متزوج; MARRIED
;	SEPARATED   ; منفصل; SEPARATED
;	DIVORCED   ; مطلق; DIVORCED
;	WIDOWED   ; ارمل; WIDOWED

INSERT_UPDATE Hdfu; code [unique = true]; name[lang = ar]; name[lang = en]
;	EMAIL ; البريد الإلكتروني; Email
;	SOCIALMEDIA ; وسائل الاعلام الاجتماعية; Social media
;	INSTORE ; في المتجر; In store
;	SMS ; رسالة قصيرة; SMS
;	RECOMMENDATION ; توصية; Recomendation
;	NEWSPAPER ; جريدة; Newspaper

INSERT_UPDATE LanguagesEnum; code [unique = true]; name[lang = ar]; name[lang = en]
;	en ; الإنجليزية; English
;	ar   ; العربية; Arabic


# Media Folders
INSERT_UPDATE MediaFolder;qualifier[unique=true];path[unique=true]
;scs;scs
;images;images
;email-body;email-body
;email-attachments;email-attachments

# Media formats
INSERT_UPDATE MediaFormat;qualifier[unique=true]
;1200Wx1200H
;515Wx515H
;365Wx246H
;300Wx300H
;96Wx96H
;65Wx65H
;30Wx30H
;mobile
;tablet
;desktop
;widescreen
;Sharjahx3D

##### TEMP MEDIA FORMATS 4 scs #####
;small
;medium
;large
;xxl
;recipe

# Tax & Tax Groups
INSERT_UPDATE UserTaxGroup;code[unique=true]
;ae-taxes

INSERT_UPDATE ProductTaxGroup;code[unique=true]
;ae-vat-full

INSERT_UPDATE Tax;code[unique=true];value;currency(isocode)
;ae-vat-full;0

INSERT_UPDATE ServicelayerJob;code[unique=true];springId[unique=true]
;cartRemovalJob;cartRemovalJob
;siteMapMediaJob;siteMapMediaJob

# Deactivate Frontend Restriction on category by default for perfomance purposes
UPDATE SearchRestriction;code[unique=true];active[default=false]
;Frontend_RestrictedCategory

INSERT_UPDATE ComponentTypeGroups2ComponentType;source(code)[unique=true];target(code)[unique=true] 
;navigation;SharjahCMSLinkComponent

#% import  de.hybris.platform.jalo.flexiblesearch.*;
INSERT_UPDATE Script;scriptType(code);active;code[unique=true];version[unique=true];content;
#% if: FlexibleSearch.getInstance().search("SELECT {pk} FROM {Script} WHERE {code} = 'confirmPickup' AND {version} = '0'" ,de.hybris.platform.jalo.Item.class).getCount() == 0
;GROOVY;true;confirmPickup;0;"import de.hybris.platform.basecommerce.enums.ConsignmentStatus; import com.hybris.cockpitng.actions.ActionContext; import com.hybris.cockpitng.actions.ActionResult; import de.hybris.platform.ordersplitting.model.ConsignmentModel; import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel; import de.hybris.platform.processengine.BusinessProcessService; import com.hybris.cockpitng.util.BackofficeSpringUtil; import com.hybris.cockpitng.actions.CockpitAction; import com.hybris.cockpitng.dataaccess.facades.object.ObjectFacade; import com.hybris.cockpitng.dataaccess.facades.object.exceptions.ObjectSavingException;  public class PickupConfirmationAction implements CockpitAction {        public ActionResult perform(final ActionContext ctx)      {         final Object data = ctx.getData();         if(data instanceof ConsignmentModel){             try{                 ((ConsignmentModel)data).setStatus(ConsignmentStatus.PICKUP_COMPLETE);                 getObjectFacade().save(data);                 for (final ConsignmentProcessModel process : ((ConsignmentModel)data).getConsignmentProcesses())                 {                     getBusinessProcessService().triggerEvent(process.getCode() + ""_ConsignmentPickup"");                 }             }             catch(final ObjectSavingException e){                 throw new RuntimeException(e);             };             return new ActionResult(ActionResult.SUCCESS);         };         return new ActionResult(ActionResult.ERROR);     };      public boolean canPerform(final ActionContext ctx)     {         return (ctx.getData() instanceof ConsignmentModel) && ((ConsignmentModel)ctx.getData()).getStatus().equals(ConsignmentStatus.READY_FOR_PICKUP)     };      public boolean needsConfirmation(final ActionContext ctx)     {         return false;     };      public String getConfirmationMessage(final ActionContext ctx)     {         return ""Are you sure?"";     };       public ObjectFacade getObjectFacade(){         return (ObjectFacade) BackofficeSpringUtil.getBean(""objectFacade"");     };       protected BusinessProcessService getBusinessProcessService()     {         return BackofficeSpringUtil.getBean(""businessProcessService"", BusinessProcessService.class);     } }";
#% endif:
