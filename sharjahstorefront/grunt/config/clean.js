/*global require, module,  __dirname, console*/
module.exports = {
	build: {
		src: ['<%= store.baseDist %>**/*', '!<%= store.baseDist %>addons/**/*']
	},
	css: {
		src: ['<%= store.baseDist %>**/*.{css,css.map}', '!<%= store.baseDist %>addons/**/*.css']
	},
	cssdev: {
		src: ['<%= store.baseDist %>**/*.{css,css.map}', '!<%= store.baseDist %>addons/**/*.css', '!<%= store.baseDist %>**/*.min.{css,css.map}']
	},
	fonts: {
		src: ['<%= store.baseDist %>**/*.{ttf,woff,woff2}', '!<%= store.baseDist %>addons/**/*.{ttf,woff,woff2}']
	},
	images: {
		src: ['<%= store.baseDist %>**/*.{png,jpg,gif,ico,svg}', '!<%= store.baseDist %>addons/**/*.{png,jpg,gif,ico,svg}']
	},
	js: {
		src: ['<%= store.baseDist %>**/*.{js,js.map}', '!<%= store.baseDist %>addons/**/*.js']
	},
	jsdev: {
		src: ['<%= store.baseDist %>**/*.{js,js.map}', '!<%= store.baseDist %>addons/**/*.js', '!<%= store.baseDist %>**/*.min.{js,js.map}']
	}
};
