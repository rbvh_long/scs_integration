/*global require, module,  __dirname, console*/
module.exports = {
	global: {
    files: [{
      expand: true,
      cwd: '<%= store.baseSrc %>',
      src: '<%= store.src.js.targets %>',
      dest: '<%= store.baseDist %>',
      rename: function(dest, src) {
        var nsrc = src.replace(new RegExp("/themes/(.*)/less"), "/theme-$1/css");
        return dest + nsrc;
      }
    }],
		options: {
			separator: ';',
      sourceMap: true
		}
	}
};
