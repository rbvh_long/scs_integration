module.exports = function(grunt) {
	grunt.registerTask('buildUglifyTargets', 'task to generate the themes folder paths', function() {
		var path = require('path');
		var fs = require('fs');
		var pkg = grunt.config('pkg');
		var store = grunt.config('store');
		var ttSelected = store.theme.themeType.selected;
		var ttListed = store.theme.themeType.listed;
		var key = '',
			obj = {},
			taskName = '',
			taskNameMin = '',
			uglifyNoMin = {},
			uglifyMin = {};
		store.src.js.bundles = [];
		store.src.js.targets = [];

		function msgColor(msg, severity) {
			var color = '';
			var wrapper = '';
			switch (severity) {
				case 1:
					color = 'yellow';
					wrapper = '\n' +
						'\t\t!!!!!!!!!!!!!!!!! ' [color].bold + msg[color].bold + ' !!!!!!!!!!!!!!!!!\n' [color].bold;
					break;
				case 2:
					color = 'red';
					wrapper = '\n' +
						'\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n' [color].bold +
						'\t\t ' [color].bold + msg[color].bold + ' \n' [color].bold +
						'\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n' [color].bold;
					break;
				default:
					color = 'green';
					wrapper = '\n' +
						'\t\t' [color].bold + msg[color].bold + '\n' [color].bold;
			}
			return wrapper;
		}

		if (ttSelected.indexOf('all') === -1) {

			//= Check if the json file with all the js configuration exists
			if (!fs.existsSync('config.js.' + ttSelected + '.json')) {
				grunt.log.writeln(msgColor('Info : File "config.js.' + ttSelected + '.json" doesn\'t exists !', 1));
				grunt.fail.warn(msgColor('Advice : create a file named "config.js.' + ttSelected + '.json" and see the CONFLUENCE documentation to get the file structure', 2));
			} else {
				//= Creation of the JS file list to compile
				key = ttSelected;
				obj[key] = grunt.file.readJSON('config.js.' + ttSelected + '.json');

				//= Get every theme bundle defined in the JS configuration file
				for (var themeBundle in obj[key]) {
					var bundle = obj[key][themeBundle].bundle;
					taskName = key + themeBundle.replace(/\b\w/g, function(l) {
						return l.toUpperCase();
					});
					taskNameMin = taskName + 'Min';
					uglifyNoMin = {
						files: [{
							cwd: '.',
							src: bundle,
							dest: '<%=store.baseDist %>' + key + '/common/js/' + themeBundle + '.js',
							rename: function(dest, src) {
								var nsrc = src;
								if (src.indexOf('common') === -1) {
									nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js");
								} else {
									nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js");
								}
								nsrc = nsrc.replace('.js', '.min.js');
								return dest + nsrc;
							}
						}],
						options: {
							beautify: {
								indent_level: 2
							},
							preserveComments: 'all',
							sourceMap: {
								includeSources: true
							}
						}
					};
					uglifyMin = {
						files: [{
							cwd: '.',
							src: bundle,
							dest: '<%=store.baseDist %>' + key + '/common/js/' + themeBundle + '.min.js',
							rename: function(dest, src) {
								var nsrc = src;
								if (src.indexOf('common') === -1) {
									nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js");
								} else {
									nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js");
								}
								nsrc = nsrc.replace('.js', '.min.js');
								return dest + nsrc;
							}
						}],
						options: {
							compress: {
								drop_console: false,
								dead_code: true,
								drop_debugger: true,
								conditionals: true,
							},
							report: 'gzip'
						}
					};
					// console.log('==============');
					// grunt.fail.warn('stop');
					grunt.config.set('uglify.' + taskName, uglifyNoMin);
					grunt.config.set('uglify.' + taskNameMin, uglifyMin);
				}
			}
		} else {
			for (var oneTheme in ttListed) {
				//= Check if the json file with all the js configuration exists
				if (!fs.existsSync('config.js.' + ttListed[oneTheme] + '.json')) {
					grunt.log.writeln(msgColor('Info : File "config.js.' + ttListed[oneTheme] + '.json" doesn\'t exists !', 1));
					grunt.fail.warn(msgColor('Advice : create a file named "config.js.' + ttListed[oneTheme] + '.json" and see the CONFLUENCE documentation to get the file structure', 2));
				} else {

					//= Creation of the JS file list to compile
					key = ttListed[oneTheme];
					obj[key] = grunt.file.readJSON('config.js.' + ttListed[oneTheme] + '.json');


					//= Get every theme bundle defined in the JS configuration file
					for (var thisThemeBundle in obj[key]) {
						grunt.log.writeln('===> ' + key);
						grunt.log.writeln('===> ' + thisThemeBundle);
						taskName = key + thisThemeBundle.replace(/\b\w/g, function(l) {
							return l.toUpperCase();
						});
						taskNameMin = taskName + 'Min';
						uglifyNoMin = {
							files: [{
								cwd: '.',
								src: obj[key][thisThemeBundle].bundle,
								dest: '<%=store.baseDist %>' + key + '/common/js/' + thisThemeBundle + '.js',
								rename: function(dest, src) {
									var nsrc = src;
									if (src.indexOf('common') === -1) {
										nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js");
									} else {
										nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js");
									}
									nsrc = nsrc.replace('.js', '.min.js');
									return dest + nsrc;
								}
							}],
							options: {
								beautify: {
									indent_level: 2
								},
								preserveComments: 'all',
								sourceMap: {
									includeSources: true,
									root: '<%=store.baseSrc %>'
								}
							}
						};
						uglifyMin = {
							files: [{
								cwd: '.',
								src: obj[key][thisThemeBundle].bundle,
								dest: '<%=store.baseDist %>' + key + '/common/js/' + thisThemeBundle + '.min.js',
								rename: function(dest, src) {
									var nsrc = src;
									if (src.indexOf('common') === -1) {
										nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js");
									} else {
										nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js");
									}
									nsrc = nsrc.replace('.js', '.min.js');
									return dest + nsrc;
								}
							}],
							options: {
								compress: {
									drop_console: true,
									dead_code: true,
									drop_debugger: true,
									conditionals: true,
								},
								report: 'gzip'
							}
						};
						grunt.log.writeln(JSON.stringify(obj[key][thisThemeBundle].bundle, null, 4));
						grunt.config.set('uglify.' + taskName, uglifyNoMin);
						grunt.config.set('uglify.' + taskNameMin, uglifyMin);
						// console.log(JSON.stringify(grunt.config.get('uglify'), null, 4));
					}
				}
			}

		}

		store.src.js.bundles.push(obj);
		grunt.config.set('store', store);
		if (grunt.cli.tasks[0] === 'buildjs:dev') {
			grunt.task.run('uglify:' + taskName);
		} else {
			grunt.task.run('uglify');
		}
		// grunt.fail.warn('stop');

		//= For debugging
		// if(grunt.option('debug')) {
		// 	console.log('==============');
		// 	console.log('store : \n\t\t');
		// 	console.log(JSON.stringify(grunt.config.get('uglify'), null, 4));
		// 	console.log('==============');
		// 	grunt.fail.warn('stop');
		// }

	});
};
