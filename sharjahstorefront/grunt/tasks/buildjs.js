	module.exports = function(grunt) {
		var pkg = grunt.config('pkg');
		var store = grunt.config('store');


		if (store.theme.themeType.selected.length > 0) {
			grunt.registerTask('buildjs', ['buildJsHintTargets', 'jshint', 'clean:js', 'buildUglifyTargets', 'sync:syncjs']);
		} else {
			grunt.registerTask('buildjs', ['choosethemetype', 'buildJsHintTargets', 'jshint', 'clean:js', 'buildUglifyTargets', 'uglify', 'sync:syncjs']);
			grunt.registerTask('buildjs:dev', ['choosethemetype', 'buildJsHintTargets', 'jshint', 'clean:jsdev', 'buildUglifyTargets', 'uglify:dev', 'sync:syncjs']);
		}
		// //= For debugging
		// if (grunt.option('debug')) {
		// 	console.log('==============');
		// 	console.log('store.theme.themeType.selected : \n\t\t');
		// 	console.log(JSON.stringify(store.theme.themeType.selected));
		// 	console.log('==============');
		// 	grunt.fail.warn('stop');
		// }
	};
