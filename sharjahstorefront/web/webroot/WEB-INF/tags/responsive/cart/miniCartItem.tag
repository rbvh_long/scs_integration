<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="updateUrl" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:url value="${entry.product.url}" var="entryProductUrl"/>

<div class="minicartItem">
	<%-- IMAGE --%>
	<div class="minicartItem--image">
		<a href="${entryProductUrl}">
			<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
		</a>
	</div>
	<div class="minicartItem--infos">
		<div class="minicartItem--infos-part-1">
			<%-- NAME --%>
			<div class="minicartItem--name"><a href="${entryProductUrl}">${entry.product.name}</a></div>
			<%-- PROMO TEXT --%>
			<c:if test="${entry.product.discountPrice ne null}">
				<div class="cartItem--discount-text">Discount text</div>
			</c:if>
			<%-- CONFIG --%>
			<div class="cartItem--config">
				<c:forEach items="${entry.product.baseOptions}" var="baseOptions">
					<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
						<c:if test="${baseOptionQualifier.qualifier eq 'style' and not empty baseOptionQualifier.image.url}">
							<div class="itemColor">
								<span class="label"><spring:theme code="product.variants.colour"/></span>
								<img src="${baseOptionQualifier.image.url}" alt="${baseOptionQualifier.value}" title="${baseOptionQualifier.value}"/>
							</div>
						</c:if>
						<c:if test="${baseOptionQualifier.qualifier eq 'size'}">
							<div class="itemSize">
								<span class="label"><spring:theme code="product.variants.size"/></span>
									${baseOptionQualifier.value}
							</div>
						</c:if>
					</c:forEach>
				</c:forEach>
			</div>
			<%-- UNIT PRICE AND QANTITY --%>
			<div class="minicartItem--price price">
				<c:if test="${entry.product.discountPrice ne null}">
					<div class="price--sticker">${entry.product.percentageDiscount}<spring:theme code="product.percentage.off"/></div>
				</c:if>
				<div class="price--line">
					<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
						<product:productPricePanel product="${entry.product}" noStrikeCurrency="true"/>
					</ycommerce:testId>
				</div>
			</div>
			<%-- STOCK --%>
			<div class="minicartItem--stock">
				<div class="minicartItem--stock-ok icon icon--check-circle-jungle-green">
					<spring:theme code="cart.product.stock.ok"/>
				</div>
			</div>
		</div>
		<%-- <div class="minicartItem--test" data-productCode="${entry.product.code}" data-unit="">${entry.quantity}</div>--%>
		<%-- <c:set var="productquantity" value="${entry.quantity}"/> --%>
		<div class="minicartItem--infos-part-2">
			<div class="minicartItem--qty">
				<div class="minicartItem--qty-selector">
					<%-- QTY Selector --%>
					<product:productCartQuantitySelector product="${entry.product}" entry="${entry}" updateUrl="${updateUrl}" />
				</div>
				<%-- REMOVE --%>
				<c:if test="${entry.updateable}" >
					<div class="minicartItem--delete delete remove-item remove-mini-cart-entry-button visible-xs visible-sm" id="removeEntry_${entry.entryNumber}"
								data-mini-cart-url="${updateUrl}" data-productCode="${entry.product.code}" data-entryNumber="${entry.entryNumber}"
								data-quantity="${productquantity}" data-productName="${entry.product.name}"
								data-productBrand="${entry.product.brandName}" data-productPrice="${entry.totalPrice.value}">
						<span class="icon icon--garbage-carnation">&nbsp;</span>
					</div>
				</c:if>
			</div>
			<div class="minicartItem--total price--total entry${entry.entryNumber}" data-productPrice="${entry.product.discountPrice ne null ? entry.product.discountPrice.value : entry.basePrice.value}"><format:priceTotalDom priceData="${entry.totalPrice}" /></div>
			<c:if test="${entry.updateable}" >
				<div class="minicartItem--delete delete remove-item remove-mini-cart-entry-button visible-md visible-lg" id="removeEntry_${entry.entryNumber}"
							data-mini-cart-url="${updateUrl}" data-productCode="${entry.product.code}" data-entryNumber="${entry.entryNumber}"
							data-quantity="${productquantity}" data-productName="${entry.product.name}"
							data-productBrand="${entry.product.brandName}" data-productPrice="${entry.totalPrice.value}">
					<span class="icon icon--garbage">&nbsp;</span>
				</div>
			</c:if>
		</div>
	</div>
</div>
