<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>" />
 <c:url value="/cart/update" var="cartUpdateFormAction" />

<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">




	<c:if test="${not empty entry.statusSummaryMap}" >
		<c:set var="errorCount" value="${entry.statusSummaryMap.get(errorStatus)}"/>
		<c:if test="${not empty errorCount && errorCount > 0}" >
				<div class="notification has-error">
					<spring:theme code="basket.error.invalid.configuration" arguments="${errorCount}"/>
					<a href="<c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" />" >
						<spring:theme code="basket.error.invalid.configuration.edit"/>
					</a>
				</div>
		</c:if>
	</c:if>
	<c:set var="showEditableGridClass" value=""/>
	<c:url value="${entry.product.url}" var="productUrl"/>
	<div class="cartItem">
		<%-- IMAGE --%>
		<div class="cartItem--image">
			<a href="${productUrl}">
				<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
			</a>
		</div>
		<div class="cartItem--infos">
			<div class="cartItem--infos-part-1">
				<div class="cartItem--subinfos">
					<%-- NAME --%>
					<div class="cartItem--name"><a href="${productUrl}">${entry.product.name}</a></div>
					<%-- PROMO TEXT --%>
					<%--<c:if test="${entry.product.discountMsg ne null}">
						<div class="cartItem--discount-text">Discount text</div>
					</c:if>
					--%>
					<c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
						<c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
							<c:set var="displayed" value="false"/>
							<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
								<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
									<c:set var="displayed" value="true"/>
									<div class="cartItem--discount-text">
										<ycommerce:testId code="cart_potentialPromotion_label">
										${fn:escapeXml(promotion.description)}
										</ycommerce:testId>
									</div>
								</c:if>
							</c:forEach>
						</c:forEach>
					</c:if>
					<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
						<c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
							<c:set var="displayed" value="false"/>
							<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
								<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
									<c:set var="displayed" value="true"/>
									<div class="cartItem--discount-text">
										<ycommerce:testId code="cart_appliedPromotion_label">
										${fn:escapeXml(promotion.description)}
										</ycommerce:testId>
									</div>
								</c:if>
							</c:forEach>
						</c:forEach>
					</c:if>
					<%-- STOCK --%>
					<div class="cartItem--stock visible-md visible-lg">
						<c:set var="entryStock" value="${entry.product.stock.stockLevelStatus.code}"/>
						<c:choose>
							<c:when test="${not empty entryStock and entryStock ne 'outOfStock' or entry.product.multidimensional}">
								<div class="cartItem--stock-ok icon icon--check-circle-jungle-green">
									<spring:theme code="product.variants.in.stock"/>
								</div>
							</c:when>
							<c:otherwise>
								<div class="cartItem--stock-ko icon icon--close">
									<spring:theme code="product.variants.out.of.stock"/>
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="item__configuration cartItem--config product-config visible-md visible-lg">
					<c:if test="${not empty entry.classifProductCartData}">
						<c:forEach items="${entry.classifProductCartData}" var="classification">
							<c:if test="${fn:toLowerCase(classification.name) eq 'configuration'}">
								<c:if test="${not empty classification.features}">
									<c:url value="/cart/updateConfig" var="cartUpdateConfigFormAction" />
									<form:form id="updateCartConfigForm${entry.entryNumber}" action="${cartUpdateConfigFormAction}" method="post" class="update_cart_form product-config--form" modelAttribute="updateConfigForm">
										<p class="product-config--title"><spring:theme code="addToCart.text.configuration"/></p>
										<div class="product-config--select-wrapper">
											<c:forEach items="${classification.features}" var="feature">
												<form:select id="productConfOption" onchange="submit()" path="configuration" class="product-config--select js-activeMe">
													<c:forEach items="${feature.featureValues}" var="featureValue" varStatus="status">
														<option value="${fn:escapeXml(featureValue.value)}" ${featureValue.value == entry.entryConf ? 'selected="selected"' : ''}>${fn:escapeXml(featureValue.value)}</option>
													</c:forEach>
												</form:select>
											</c:forEach>
										</div>
										<input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
										<input type="hidden" name="productCode" value="${entry.product.code}"/>
									</form:form>
								</c:if>
							</c:if>
						</c:forEach>
					</c:if>
				</div>
				<%-- UNIT PRICE AND QANTITY --%>
				<div class="cartItem--price price">
					<c:if test="${entry.product.discountPrice ne null}">
						<div class="price--sticker">${entry.product.percentageDiscount}<spring:theme code="product.percentage.off"/></div>
					</c:if>
					<div class="price--line">
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${entry.product}" noStrikeCurrency="true"/>
						</ycommerce:testId>
					</div>
				</div>
				<%-- STOCK --%>
				<div class="cartItem--stock visible-xs visible-sm">
					<div class="cartItem--stock-ok icon icon--check-circle-jungle-green">
						<spring:theme code="cart.product.stock.ok"/>
					</div>
				</div>
			</div>
			<div class="cartItem--infos-part-2">
				<%-- Configuration selector --%>
				<c:if test="${entry.product.configurable}">
					<div class="cartItem--config">
						<c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
						<div class="item__configurations">
							<c:forEach var="config" items="${entry.configurationInfos}">
								<c:set var="style" value=""/>
								<c:if test="${config.status eq errorStatus}">
									<c:set var="style" value="color:red"/>
								</c:if>
								<div class="item__configuration--entry" style="${style}">
								<div class="row">
								<div class="item__configuration--name col-sm-4">
								${config.configurationLabel}
								<c:if test="${not empty config.configurationLabel}">:</c:if>
								</div>
								<div class="item__configuration--value col-sm-8">
								${config.configurationValue}
								</div>
								</div>
								</div>
							</c:forEach>
						</div>
						<c:if test="${not empty entry.configurationInfos}">
							<div class="item__configurations--edit">
								<a class="btn" href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
							</div>
						</c:if>
					</div>
				</c:if>

				<div class="cartItem--qty-selection-row">
					<form:form id="updateCartForm${loop.index}"
							action="${cartUpdateFormAction}"
							method="post"
							commandName="updateQuantityForm${entry.entryNumber}"
							class="js-qty-form${loop.index} update_cart_form">
						<input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
						<input type="hidden" name="productCode" value="${entry.product.code}"/>
						<input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
						<input type="hidden" name="quantity" value="${entry.quantity}"/>
						<div class="cartItem--qty">
							<div class="cartItem--qty-selector">
							<%-- QTY Selector --%>
								<product:productCartPageQuantitySelector product="${entry.product}" entry="${entry}" updateUrl="${updateUrl}"/>
							</div>
							<%-- REMOVE --%>
							<c:if test="${entry.updateable}">
								<div class="cartItem--delete delete remove-item remove-mini-cart-entry-button visible-xs visible-sm"
								id="removeEntry_${entry.entryNumber}"
								data-mini-cart-url="${updateUrl}" data-productCode="${entry.product.code}"
								data-entryNumber="${entry.entryNumber}"
								data-quantity="${productquantity}" data-productName="${entry.product.name}"
								data-productBrand="${entry.product.brandName}" data-productPrice="${entry.totalPrice.value}">
									<span class="icon icon--garbage-carnation js-remove-entry-button" id="removeEntry_${loop.index}">&nbsp;</span>
								</div>
							</c:if>
						</div>
					</form:form>

					<div class="cartItem--total price--total"><format:priceTotalDom priceData="${entry.totalPrice}"/></div>
					<c:if test="${entry.updateable}">
						<div class="cartItem--delete delete remove-item remove-mini-cart-entry-button visible-md visible-lg"
						id="removeEntry_${entry.entryNumber}"
						data-mini-cart-url="${updateUrl}" data-productCode="${entry.product.code}"
						data-entryNumber="${entry.entryNumber}"
						data-quantity="${productquantity}" data-productName="${entry.product.name}"
						data-productBrand="${entry.product.brandName}" data-productPrice="${entry.totalPrice.value}">
							<span class="icon icon--garbage js-remove-entry-button" id="removeEntry_${loop.index}">&nbsp;</span>
						</div>
					</c:if>
				</div>
				<c:set var="isForceInStock"
					   value="${entry.product.stock.stockLevelStatus.code eq 'inStock' and empty entry.product.stock.stockLevel}" />
				<c:choose>
					<c:when test="${isForceInStock}">
						<c:set var="maxQty" value="FORCE_IN_STOCK" />
					</c:when>
					<c:otherwise>
						<c:set var="maxQty" value="${entry.product.stock.stockLevel}" />
					</c:otherwise>
				</c:choose>
<%-- 				<c:if test="${!isForceInStock}"> --%>
<!-- 					<div class="cartItem--max-quantity-message"> -->
<%-- 						<spring:theme code="product.variants.quantity.limit" arguments="${maxQty}" /> --%>
<!-- 					</div> -->
<%-- 				</c:if> --%>
			</div>
		</div>
	</div>

</c:forEach>
<product:productOrderFormJQueryTemplates />
<%-- <storepickup:pickupStorePopup /> --%>
