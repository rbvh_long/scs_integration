<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>




<c:set var="quantity" value="1" />
<c:set var="minQty" value="1" />
<input type="hidden" maxlength="3" size="1" id="qty" name="qty" value="${minQty}">

<div class="qty-selector js-qty-selector">
	<div class="qty-selector--btn qty-selector--minus minus js-qty-selector-minus" ${quantity == 1 ? 'disabled="disabled"' : ''}
			data-productCode="${product.code}" id="removeEntry_${product.code}" data-entryNumber="${product.code}">
		<span class="icon icon--minus-carnation"></span>
	</div>
	<product:quantitySelector id="pdpAddtoCartInput" name="pdpAddtoCartInput" product="${product}" isForceInStock="${isForceInStock}" quantity="${quantity}" readonly="readonly='readonly'" customClass="qty-selector--input"/>
	<div class="qty-selector--btn qty-selector--plus plus js-qty-selector-plus"
			data-productCode="${product.code}" id="removeEntry_${product.code}"
			data-mini-cart-url="${updateUrl}" data-entryNumber="${product.code}">
		<span class="icon icon--plus-carnation"></span>
	</div>
</div>
