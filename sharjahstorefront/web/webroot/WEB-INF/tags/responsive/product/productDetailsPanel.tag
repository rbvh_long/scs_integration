<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="product">
	<div class="product-details">
		<div class= "product-details--carousel">
			<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
				<p class="product-details--carousel-name">${fn:escapeXml(product.name)}</p>
			</ycommerce:testId>
	<%-- 	<product:productReviewSummary product="${product}" showLinks="true"/> --%>




			<product:productImagePanel galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}"
				product="${product}" />
		</div>
		<div class="product-details--info">
			<!-- <div class="product-details--info"> -->

				<div class="product-details--info--first-section">
					<div class="product-details--info--first-section-price price visible-md visible-lg">
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" noStrikeCurrency="true"/>
						</ycommerce:testId>
					</div>
					<a class="product-details--info--first-section-link" href="#description"><u><spring:theme code="product.link.description"/></u></a>
					<div class="product-details--info--first-section-price price visible-xs visible-sm">
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" noStrikeCurrency="true"/>
						</ycommerce:testId>
					</div>

				<%-- 	<div class="product-details--info-description">
						${fn:escapeXml(product.summary)}
					</div>	 --%>
				</div>


				<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
					<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
				</cms:pageSlot>

				<div class="product-details--info--second-section">
					<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
						<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
					</cms:pageSlot>
				</div>
			<!-- </div> -->
<!-- 			<div class="product-details--info--brand"> -->
<%-- 				<img class="product-details--info--brand-img" src="${commonResourcePath}/images/puck-logo.png" /> --%>
<%-- 				<a class="product-details--info--brand-link" href=""><spring:theme code="productDetails.text.puck.products.link"/></a> --%>
<!-- 			</div> -->
		<%-- BRAND LOGO --%>
			<c:if test="${not empty product.brandName}">
				<div class="product-details--info--brand">
					${product.brandName}
					<a class="product-details--info--brand-img" href="${pageContext.request.contextPath}/c/${product.brandCode}"><img src="${product.brandLogo.url}" /></a>
				</div>
			</c:if>
		</div>
	</div>
</div>
