<%@ attribute name="id" required="true" type="java.lang.String" description="Id of the SVG element" %>
<%@ attribute name="role" required="true" type="java.lang.String" description="Role of the SVG element" %>
<%@ attribute name="name" required="false" type="java.lang.String" description="Name of the SVG element" %>
<%@ attribute name="width" required="false" type="java.lang.String" description="Width of the SVG element" %>
<%@ attribute name="height" required="false" type="java.lang.String" description="Height of the SVG element" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty name}">
	<c:set var="name" value="${id}" />
</c:if>
<c:if test="${empty width}">
	<c:set var="width" value="45px" />
</c:if>
<c:if test="${empty height}">
	<c:set var="height" value="45px" />
</c:if>
${name}
<svg title="${name}" role="${role}" width="${width}" height="${height}">
		<use xlink:href="/_ui/shared/svg/${name}.svg"/>
</svg>
