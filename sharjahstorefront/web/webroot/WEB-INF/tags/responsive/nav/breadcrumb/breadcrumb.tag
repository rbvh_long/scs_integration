<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="breadcrumbs" required="true" type="java.util.List"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="/" var="homeUrl" />

<ol class="breadcrumb">
	<li>
		<a href="${homeUrl}"><span class="icon icon--home-carnation icon--inline-block">&nbsp;</span><spring:theme code="breadcrumb.home" /><span class="icon icon--arrow-thin-carnation">&nbsp;</span></a>
	</li>
	<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
		<c:url value="${breadcrumb.url}" var="breadcrumbUrl" />
		<c:choose>
			<c:when test="${status.last}">
				<li class="active">
					<a class="disabled">${fn:escapeXml(breadcrumb.name)}</a>
				</li>
			</c:when>
			<c:when test="${breadcrumb.url eq '#'}">
				<li>
					<a class="disabled">
						${fn:escapeXml(breadcrumb.name)}
						<span class="icon icon--arrow-thin-carnation">&nbsp;</span>
					</a>
				</li>
			</c:when>
			<c:otherwise>
				<li>
					<a href="${breadcrumbUrl}">
						${fn:escapeXml(breadcrumb.name)}
						<span class="icon icon--arrow-thin-carnation">&nbsp;</span>
					</a>
				</li>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</ol>
