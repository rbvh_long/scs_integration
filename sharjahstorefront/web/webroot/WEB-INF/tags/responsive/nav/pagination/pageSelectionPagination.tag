<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ attribute name="themeMsgKey" required="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


	<c:set var="currentPage" value="${searchPageData.pagination.currentPage}"/>
	<c:set var="numberOfPages" value="${searchPageData.pagination.numberOfPages}"/>
	<c:set var="hasPreviousPage" value="${currentPage > 0}"/>
	<c:set var="showFirstPage" value="${currentPage + 1 > 1}"/>
	<c:set var="hasNextPage" value="${numberOfPages > 5 and (currentPage + 1 ne numberOfPages)}"/>
	<c:forEach items="${searchPageData.sorts}" var="sort">
		<c:if test="${sort.selected}">
			<c:set value="${sort.code}" var="sortCode"/>
		</c:if>
	</c:forEach>
	
	<c:if test="${hasPreviousPage && showFirstPage}">
			<spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
			    <%-- The currentPage is already decrease by 1 according to display --%>
				<spring:param name="page" value="${currentPage-1}"/>
				<c:if test="${not empty searchPageData.sorts && not empty sortCode}">
				    <spring:param name="sort" value="${sortCode}"/>
				</c:if>
			</spring:url>
		<li class="prev" onclick="location.href='${fn:replace(previousPageUrl, fn:escapeXml('&page=0'), '')}';">
			<ycommerce:testId code="searchResults_previousPage_link">
				<!-- SEO request to remove &page=1 for the first page -->		
				<a href="${fn:replace(previousPageUrl, fn:escapeXml('&page=0'), '')}" rel="prev">
					&lt; <spring:theme code="pickup.pagination.previous"/>
				</a>
			</ycommerce:testId>
		</li>
	</c:if>

	<c:if test="${currentPage >= 4}">
			<spring:url value="${searchUrl}" var="firstPageUrl" htmlEscape="true">
				<spring:param name="page" value="1"/>
				  <c:if test="${not empty searchPageData.sorts && not empty sortCode}">
				    <spring:param name="sort" value="${sortCode}"/>
				  </c:if>
			</spring:url>
		<li class="first" onclick="location.href='${fn:replace(firstPageUrl, fn:escapeXml('&page=1'), '')}';">
			<ycommerce:testId code="firstPage_link">
				<!-- SEO request to remove &page=1 for the first page -->
				<a href="${fn:replace(firstPageUrl, fn:escapeXml('&page=1'), '')}" rel="prev">1</a>
			</ycommerce:testId>
		</li>
		<li class="dots">...</li>
	</c:if>

	<%-- 	For each paging, we have a starting step and a final step.
			For each selected page, we calculate the interval of paging.
			Examples for conditions below:
				Search result page producing 20 pages of items.
				
					Condition 1 >>>
						if number of selected page is 13 then >>
						currentPage is 12, numberOfPages is 20
						we only need to calculate the interval of pages beginPage and endPage for this selection
						(currentPage = 12) is pair and (currentPage = 12) divided by 2 is pair
							so the interval starts with 13 and ends by 17, we are going to engage the loop and display the selected page on bold style.
								PRECEDENT 1 ...*13* 14 15 16 17...20 SUIVANT
								
					Condition 2 >>>
						if number of selected page is 15 then >>
						currentPage is 14, numberOfPages is 20
						we only need to calculate the interval of pages beginPage and endPage for this selection
						(currentPage = 14) is pair and (currentPage = 14) divided by 2 is odd
							so the interval starts with 13 and ends by 17, we are going to engage the loop and display the selected page on bold style.
								PRECEDENT 1 ...13 14 *15* 16 17...20 SUIVANT
								
					Condition 3 >>>
						if number of selected page is 2 then >>
						currentPage is 1, numberOfPages is 20
						we only need to calculate the interval of pages beginPage and endPage for this selection
						(currentPage = 1) is odd and (currentPage - 1 = 0) divided by 2 is pair
							so the interval starts with 1 and ends by 5, we are going to engage the loop and display the selected page on bold style.
								PRECEDENT 1 *2* 3 4 5 ...20 SUIVANT
					
					Condition 4 >>>
						if number of selected page is 4 then >>
						currentPage is 3, numberOfPages is 20
						we only need to calculate the interval of pages beginPage and endPage for this selection
						(currentPage = 3) is odd and (currentPage - 1 = 2) divided by 2 is odd
							so the interval starts with 1 and ends by 5, we are going to engage the loop and display the selected page on bold style.
								PRECEDENT 1 2 3 *4* 5...20 SUIVANT
	 --%>
	<c:choose>
		<%-- Condition 1 --%>
		<c:when test="${currentPage mod 2 eq 0 and (currentPage / 2) mod 2 eq 0}">
			<c:set var="beginPage" value="${currentPage + 1}"/>
			<c:set var="endPage" value="${(numberOfPages ge (currentPage + 5))?(currentPage + 5):numberOfPages}"/>
		</c:when>
		<%-- Condition 2 --%>
		<c:when test="${currentPage mod 2 eq 0 and (currentPage / 2) mod 2 eq 1}">
			<c:set var="beginPage" value="${currentPage - 1}"/>
			<c:set var="endPage" value="${(numberOfPages ge (currentPage + 3))?(currentPage + 3):numberOfPages}"/>
		</c:when>
		<%-- Condition 3 --%>
		<c:when test="${currentPage mod 2 eq 1 and ((currentPage - 1) / 2) mod 2 eq 0}">
			<c:set var="beginPage" value="${currentPage}"/>
			<c:set var="endPage" value="${(numberOfPages ge (currentPage + 4))?(currentPage + 4):numberOfPages}"/>
		</c:when>
		<%-- Condition 4 --%>
		<c:when test="${currentPage mod 2 eq 1 and ((currentPage - 1) / 2) mod 2 eq 1}">
			<c:set var="beginPage" value="${currentPage - 2}"/>
			<c:set var="endPage" value="${(numberOfPages ge (currentPage + 2))?(currentPage + 2):numberOfPages}"/>
		</c:when>
	</c:choose>

	<c:set var="showEndPage" value="${((beginPage + 4) < numberOfPages)}"/>
	<c:forEach begin="${beginPage}" end="${endPage}" var="pageNumber">
		<c:choose>
			<c:when test="${currentPage + 1 ne pageNumber}">
				<spring:url value="${searchUrl}" var="pageNumberUrl"
					htmlEscape="true">
					<c:if test="${pageNumber != 1}">
						<spring:param name="page" value="${pageNumber-1}" />
					</c:if>
					<c:if test="${not empty sortCode}">
						<spring:param name="sort" value="${sortCode}" />
					</c:if>
				</spring:url>
				<ycommerce:testId code="pageNumber_link">
						<li onclick="location.href='${pageNumberUrl}';"><a href="${pageNumberUrl}" class="${pageNumber gt 9 ? 'big-number' : ''}">${pageNumber}</a></li>
				</ycommerce:testId>
			</c:when>
			<c:otherwise>
				<li class="active">${pageNumber}</li>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<c:if test="${hasNextPage}">
		<c:if test="${showEndPage}">
			<li>...</li>
		</c:if>
		<spring:url value="${searchUrl}" var="lastPageUrl" htmlEscape="true">
				<spring:param name="page" value="${numberOfPages-1}"/>
				   <c:if test="${not empty searchPageData.sorts && not empty sortCode}">
				       <spring:param name="sort" value="${sortCode}"/>
				   </c:if>
			</spring:url>
		<c:if test="${pageNumberUrl != lastPageUrl }">
		<li>
			<ycommerce:testId code="lastPage_link">
					<a href="${lastPageUrl}" rel="next" class="${numberOfPages - 1 gt 9 ? 'big-number' : ''}">${numberOfPages}</a>
			</ycommerce:testId>
		</li></c:if>
		<li class="next last">
			<spring:url value="${searchUrl}" var="nextPageUrl" htmlEscape="true">
			    <%-- The currentPage is already decrease by 1 according to display so + 2 --%>
				<spring:param name="page" value="${currentPage + 1}"/>
					<c:if test="${not empty searchPageData.sorts && not empty sortCode}">
						<spring:param name="sort" value="${sortCode}"/>
					</c:if>
			</spring:url>
			<ycommerce:testId code="searchResults_nextPage_link">
				<a href="${nextPageUrl}" rel="next">
					<spring:theme code="pickup.pagination.next"/> &gt;
				</a>
			</ycommerce:testId>
		</li>
	</c:if>
