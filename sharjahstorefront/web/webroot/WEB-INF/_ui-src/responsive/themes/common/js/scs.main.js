var SCS = {};

SCS.main = {

	/**
	 * Remove the Colorbox title, which floats invisibly over the content and impedes clicking.
	 */
	removeColorboxTitle: function() {
		$('#cboxTitle').remove();
	}

};

$(document).ready(function() {
	if($('#myAccountGenderCode').val() === 'MALE'){
		$('.personaldetails').removeClass("icon--user-circle");
		$('.personaldetails').removeClass("icon--woman-circle");
		$('.personaldetails').addClass('icon--user-circle');
	}else{
		$('.personaldetails').removeClass("icon--user-circle");
		$('.personaldetails').removeClass("icon--woman-circle");
		$('.personaldetails').addClass('icon--woman-circle');
	}

	$('#orderHistoryDataTable').DataTable({
		language: {
			paginate: {
				next: '&rsaquo;', // or '>'
				previous: '&lsaquo;' // or '<'
			}
		},
		columnDefs: [{
			targets: [0],
			orderData: [0, 1]
		}, {
			targets: [1],
			orderData: [1, 0]
		}, {
			targets: [2],
			orderData: [2, 0]
		}, {
			targets: [3],
			orderData: [3, 0]
		}]
	});

	$('#orderSortBySelectFilter').change(function() {
		if ($('#orderSortBySelectFilter').val() === "1") {
			$('#sortFilter_1').trigger("click");
		} else if ($('#orderSortBySelectFilter').val() === "2") {
			$('#sortFilter_2').trigger("click");
		} else if ($('#orderSortBySelectFilter').val() === "3") {
			$('#sortFilter_3').trigger("click");
		} else if ($('#orderSortBySelectFilter').val() === "4") {
			$('#sortFilter_4').trigger("click");
		} else {
			return;
		}
		$('#orderSortBySelectFilter').val('0');
	});

	console.log('scs.main.js : executed');

});
