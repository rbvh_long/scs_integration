ACC.wishlistorder = {
		
	_autoload: [
		"bindWishlistOrder",
		"bindWishlistOrderCancel",
		"bindWishlistOrderSubmit"
	],
	
	bindWishlistOrder: function() {
		$('.js-wishlist-add-button').on('click', function(e) { 
			console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrder : wishlist add button clicked');
			e.preventDefault();
			var config = {
					html: $('#idAddOrderToWishlist').html()
			};
			ACC.colorbox.open('', config);
			
    		

		});
	},
	bindWishlistOrderSubmit:function() {
		$(document).on("click",'#idSubmitNewWishlist',function() { 
			console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : submitting wishlist');
			var selection = $("#cboxContent #idSelectOption").val();
			if (selection === ACC.wishlist.NEW) {
				console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : wishlist name is empty');
				if ($("#cboxContent #newWishlistName").val().length === 0) {
					$("#cboxContent #newWishlistName").addClass("errorInput");
					$("#cboxContent .errorValidation").show();
					ACC.colorbox.resize();
					return false;
				} 
				else if ($("#cboxContent #newWishlistName").val().length > 50) {
					console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : wishlist name is too long');
					$("#cboxContent #newWishlistName").addClass("errorInput");
					$("#cboxContent .errorSize").show();
					ACC.colorbox.resize();
					return false;
				} 
				else {
					console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : prepare to create new wishlist');
					ACC.wishlistorder.addOrderToWishlist("/checkout/orderConfirmation/addToWishlist/?" + ACC.wishlistorder.generateAddProductParams());
					$('.js-wishlist-add-button').prop('disabled', true);
					return true;
				}
			} 
			else if (selection !== ACC.wishlist.NONE) {
				console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderSubmit : prepare to update wishlist');
				ACC.wishlistorder.addOrderToWishlist("/checkout/orderConfirmation/addToWishlist/?" + ACC.wishlistorder.generateAddProductParams());
				$('.js-wishlist-add-button').prop('disabled', true);
				return true;
			}
		}); 
	},
	generateAddProductParams: function() {
		var isNewWishlist = false;
		console.log('scs.wishlist-order.js - ACC.wishlist-order.generateAddProductParams : preparing wishlist params');
		var selectedList = $("#cboxContent #idSelectOption").val();
		if (selectedList === ACC.wishlist.NEW) {
			isNewWishlist = true;
			selectedList = $("#cboxContent #newWishlistName").val();
		}
		var params = {
			wishlistName: selectedList,
			orderCode: $("#cboxContent #orderCode").val(),
			isNewWishlist: isNewWishlist
		};
		return $.param(params);
	},
	addOrderToWishlist: function (url) {
		console.log('scs.wishlist-order.js - ACC.wishlist-order.addOrderToWishlist : sending wishlist params to server');
		$.ajax({
			url : ACC.config.encodedContextPath+ url,
			dataType : "html",
			type : "GET",
			success : function(data) {
				console.log('scs.wishlist-order.js - ACC.wishlist-order.addOrderToWishlist : sending successfull');
				
				var response = data;
				var successText = "Order successfully added to shopping list ";
				var failureText = "Error while trying to add order to shopping list ";
				var selectedList = $("#cboxContent #idSelectOption").val();
				
				if (selectedList === ACC.wishlist.NEW) {
					selectedList = $("#cboxContent #newWishlistName").val();
					successText = "Order successfully added to newly created shopping list ";
				}
				
				if(response.indexOf("success") !== -1){
					$('#cboxContent #idResult').text(successText + selectedList);
				}
				else{
					$('#cboxContent #idResult').text(failureText + selectedList);
				}
				$('#cboxContent #idResult').show();
				$('#cboxContent .wishListPopin').hide();
				ACC.colorbox.resize();
			}
		});
	},
	bindWishlistOrderCancel:function() {
		$(document).on("click",'#wishlistCancel',function(e) {
			console.log('scs.wishlist-order.js - ACC.wishlist-order.bindWishlistOrderCancel : canceling wishlist popin');
			e.preventDefault();
			ACC.colorbox.close();
		}); 
	}
};