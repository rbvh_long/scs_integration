ACC.langselector = {

	_autoload: [
		'bindLanguageSwitcher',
		'bindLanguageDisplay',
		'bindLanguageHide'
	],

	bindLanguageSwitcher: function() {
		console.log('acc.language-selector.js - ACC.langselector.bindLanguageSelector() : executed');
		$(document).on('click', '.js-lang-switch', function() {
			console.log('acc.language-selector.js - ACC.langselector.bindLanguageSelector() - click on : .js-lang-switch');
			var $el = $(this);
			var $form = $el.parents('form');
			$form.find('#langcode').val($el.data('lang-to-switch'));
			console.log('acc.language-selector.js - ACC.langselector.bindLanguageSelector() - language switched to : ' + $el.data('lang-to-switch'));
			$form.submit();
		});
	},
	bindLanguageDisplay: function() {
		console.log('acc.language-selector.js - ACC.langselector.bindLanguageDisplay() : executed');
		$(document).on('click', '.language-switcher:not(.open)', function(e) {
			console.log('acc.language-selector.js - ACC.langselector.bindLanguageDisplay() - click on : .language-switcher:not(.open)');
			e.stopPropagation();
			$(this).find('.items-wrapper').removeClass('hide');
			$(this).addClass('open');
		});
	},
	bindLanguageHide: function() {
		console.log('acc.language-selector.js - ACC.langselector.bindLanguageDisplay() : executed');
		$('.language-switcher.open').on('mouseleave', function() {
			console.log('acc.language-selector.js - ACC.langselector.bindLanguageDisplay() - mouseleave on : .language-switcher.open');
			$(this).find('.items-wrapper').addClass('hide');
		});
		$(window).on('click', function() {
			$('.language-switcher.open .items-wrapper').addClass('hide');
			$('.language-switcher.open').removeClass('open');
		});
	}
};
