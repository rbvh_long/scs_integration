ACC.device = {
	_autoload: [
		'setDeviceType',
		'setOrientationType'
	],
	setDeviceType: function() {
		console.log('ACC.device.setDeviceType()');
		var windowWidth = $(window).width();
		var screenMobile = screenMdMin.slice(0, -2);
		var screenTablet = screenMdMin.slice(0, -2);
		if (windowWidth < screenMobile) {
			ACC.device.type = 'mobile';
		} else if (windowWidth < screenTablet) {
			ACC.device.type = 'tablet';
		} else {
			ACC.device.type = 'desktop';
		}
	},
	setOrientationType: function() {
		if (typeof screen.orientation !== 'undefined') {
			ACC.device.orientation = screen.orientation.type;
		} else {
			ACC.device.orientation = 'landscape-primary';
		}
	}
};

window.addEventListener('resize', function() {
	ACC.device.setDeviceType();
});

window.addEventListener('orientationchange', function() {
	ACC.device.setOrientationType();
	ACC.navigation.mobileNavHeight();
});
