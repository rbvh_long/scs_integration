ACC.product = {

	_autoload: [
		'bindToAddToCartForm',
		'enableStorePickupButton',
		'enableVariantSelectors',
		'bindFacets'
	],

	addToCart: function(self) {
		console.log('acc.product.js - ACC.product.addToCart() : executed');
		var $this = self;
		var addUrl = $this.attr('action');
		var productCode = $this.find('[name=productCodePost]').val();
		var quantityField = $this.find('#qty').val();
		console.log('url : ', addUrl);
		console.log('productCode : ', productCode);
		console.log('quantityField : ', quantityField);
		if (typeof productCode !== 'undefined' && typeof quantityField !== 'undefined') {
			// Below a custom method with ajax method which fire an error
			 $.ajax({
			 	type: 'POST',
			 	url: addUrl,
				data: $this.serialize(),
			 	dataType: 'json',
			 	success: function(cartResult, statusText, xhr, formElement) {
			 		console.log('%c acc.product.js - ACC.product.addToCart() - ajaxForm : SUCCES', 'background-color:green; color:white; font-weight:bold;');
			 		console.log('//==== COMPLETE addToCart ===//');
			 		console.log(cartResult);
			 		console.log(statusText);
					console.log(xhr);
			 		console.log(formElement);
			 		console.log('//============================//');

			 		if (typeof ACC.minicart.updateMiniCartDisplay === 'function') {
			 			ACC.minicart.updateMiniCartDisplay();
			 		}
			 		ACC.product.displayGreenTick(self);
			 	},
			 	error: function(data) {
			 		console.log(data);
			 		console.log('%c acc.product.js - ACC.product.addToCart() - ajaxForm : ERROR', 'background-color:red; color:white; font-weight:bold;');
			 	}
			 });
		}
	},
	
	displayGreenTick: function(self) {
		var button = $(self).find('.js-enable-btn');
		$(button).children().hide();
		if($(button).children().length === 1){
			$(button).addClass("add-button-product-toCart-hidden");
	 		$(button).css({
	 			'background' : 'url(/_ui/responsive/theme-sharjah/images/svg/check-white.svg) no-repeat 0 0',
		 	    'background-color' : '#32CD32',
		 	    'background-position-x' : 'center',
		 	    'background-position-y' : 'center',
	 		});
		}
		else {
			$(button).css({
	 			'background' : 'url(/_ui/responsive/theme-sharjah/images/svg/check-white.svg) no-repeat 0 0',
		 	    'background-color' : '#32CD32',
		 	    'background-position-x' : 'center',
		 	    'background-position-y' : 'center',
		 	    'height' : '100%',
		 	    'width'  : '100%',
		 	    'border-color' : '#32CD32'
	 		});
		}
 		
 		setTimeout(function() {
 			$(button).removeAttr('style');
 			$(button).children().show();
 			$(button).removeClass("add-button-product-toCart-hidden");
 		}, 2000);
	},

	bindFacets: function() {
		console.log('acc.product.js - ACC.product.bindFacets() : executed');
		if (ACC.device.type === 'mobile') {
			$(document).on('click', '.js-show-facets', function(e) {
				e.preventDefault();
				var selectRefinementsTitle = $(this).data('selectRefinementsTitle');
				ACC.colorbox.open(selectRefinementsTitle, {
					href: '.js-product-facet',
					inline: true,
					height: window.innerHeight - $('.navigation--mobile-navbar').outerHeight() + $('#cookie-bar').outerHeight() + 'px',
					scrolling: true,
					top: $('.navigation--mobile-navbar').outerHeight() + $('#cookie-bar').outerHeight() + 'px',
					width: '100%',
					onComplete: function() {
						$('#cboxTitle').hide(); // We remove the useless colorbox title
						$("#cboxLoadedContent").css({
							'margin-top': '0px',
							'padding': '0px',
							'overflow': 'scroll',
							'width': '100%'
						});
						$(document).find('.js-product-facet .js-facet').removeClass('active)');
						$(document).on('click', '.js-product-facet .js-facet-name', function(e) {
							console.log('acc.product.js - ACC.product.bindFacets() - click on : document .js-product-facet .js-facet-name');
							e.preventDefault();
							if ($(this).parents('.js-facet').hasClass('active')) {
								$(this).parents('.js-facet').removeClass('active');
							} else {
								$('.js-product-facet .js-facet').removeClass('active');
								$(this).parents('.js-facet').addClass('active');
							}
						});
					},
					onClosed: function() {
						$(document).off('click', '.js-product-facet .js-facet-name');
						$("#cboxLoadedContent").attr('style', '');
					}
				});
			});
			$('#cboxClose').click();
		}
	},

	bindToAddToCartForm: function() {
		console.log('acc.product.js - ACC.product.bindToAddToCartForm() : executed');
		var addButtonSelectors = ['.add-button-toCart', '.add-button-product-toCart'];
		$('.add_to_cart_form').on('click', addButtonSelectors.join(', '), function(e) {
			console.log('acc.product.js - ACC.product.bindToAddToCartForm() - submit on : .add_to_cart_form');
			e.preventDefault();
			ACC.product.addToCart($(this).parents('.add_to_cart_form'));
		});
	},

	bindToAddToCartStorePickUpForm: function() {
		console.log('acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed');
		var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
		addToCartStorePickUpForm.ajaxForm({ success: ACC.product.displayAddToCartPopup });
	},

	displayAddToCartPopup: function(cartResult, statusText, xhr, formElement) {
		console.log('acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed');
		console.log('//==== COMPLETE displayAddToCartPopup ===//');
		console.log(cartResult);
		console.log(statusText);
		console.log(xhr);
		console.log(formElement);
		console.log('//============================//');

		$ajaxCallEvent = true;
		$('#addToCartLayer').remove();

		if (typeof ACC.minicart.updateMiniCartDisplay === 'function') {
			ACC.minicart.updateMiniCartDisplay();
		}
		//        var titleHeader = $('#addToCartTitle').html();
		//
		//        ACC.colorbox.open(titleHeader, {
		//            html: cartResult.addToCartLayer,
		//            width: '460px'
		//        });

		var productCode = $('[name=productCodePost]', formElement).val();
		var quantityField = $('[name=qty]', formElement).val();

		var quantity = 1;
		if (quantityField !== undefined) {
			quantity = quantityField;
		}

		var cartAnalyticsData = cartResult.cartAnalyticsData;

		var cartData = {
			'cartCode': cartAnalyticsData.cartCode,
			'productCode': productCode,
			'quantity': quantity,
			'productPrice': cartAnalyticsData.productPostPrice,
			'productName': cartAnalyticsData.productName
		};
		ACC.track.trackAddToCart(productCode, quantity, cartData);
	},

	enableAddToCartButton: function() {
		console.log('acc.product.js - ACC.product.enableAddToCartButton() : executed');
		$('.js-enable-btn').each(function() {
			if (!($(this).hasClass('outOfStock') || $(this).hasClass('out-of-stock'))) {
				$(this).removeAttr('disabled');
			}
		});
	},

	enableStorePickupButton: function() {
		console.log('acc.product.js - ACC.product.bindToAddToCartStorePickUpForm() : executed');
		$('.js-pickup-in-store-button').removeAttr('disabled');
	},

	enableVariantSelectors: function() {
		console.log('acc.product.js - ACC.product.enableVariantSelectors() : executed');
		$('.variant-select').removeAttr('disabled');
	},

	showRequest: function(arr, $form, options) {
		console.log('acc.product.js - ACC.product.showRequest() : executed');
		if ($ajaxCallEvent) {
			$ajaxCallEvent = false;
			return true;
		}
		return false;
	}

};

$(document).ready(function() {
	$ajaxCallEvent = true;
	ACC.product.enableAddToCartButton();
});

function getShowMode(showCode) {
	var showMode = '';
	if (showCode === 'All') {
		showMode = 'show=All';
	} else {
		showMode = 'show=Page';
	}
	return showMode;
}

 

function updateQueryStringParameter(url, showCode) {
	var urlFinalBuild = '';
	if (url.indexOf('?q=') !== -1) {
		if (url.indexOf('page=') !== -1) {
			url = url.replace('page=','');
			urlFinalBuild = url + '&'+ getShowMode(showCode);
		}
		else if (showCode === 'Page' && url.indexOf('show=All') !== -1) {
			urlFinalBuild = url.replace('show=All','show=Page');
		}
		else if (showCode === 'All' && url.indexOf('show=Page') !== -1) {
			urlFinalBuild = url.replace('show=Page','show=All');
		}
		else if (url.endsWith('#')) {
			url = url.substr(0, url.length - 1);
			urlFinalBuild = url + '&'+ getShowMode(showCode) + '#';
		} else {
			urlFinalBuild = url + '?'+ getShowMode(showCode);
		}
	} else {
		if (showCode === 'Page' && url.indexOf('show=All') !== -1) {
			urlFinalBuild = url.replace('show=All','show=Page');
		}
		else if (showCode === 'All' && url.indexOf('show=Page') !== -1) {
			urlFinalBuild = url.replace('show=Page','show=All');
		}
		else if (url.indexOf('?text') !== -1) {
				urlFinalBuild = url + '&show=' + showCode;
		}
		else{
			urlFinalBuild = url + '?show=' + showCode;
		}
	}
	return urlFinalBuild;
}

$(document).on('click', '.showAll', function(e) {
	var showCode = $(this).data("showcode");
	var currentUrl = window.location.href;
	 document.location.href = updateQueryStringParameter(currentUrl,showCode);
	  
});

$(document).on('click', '.showNoAll', function(e) {
	var showCode = $(this).data("showcode");
	var currentUrl = window.location.href;
	 document.location.href = updateQueryStringParameter(currentUrl,showCode);
 
});
