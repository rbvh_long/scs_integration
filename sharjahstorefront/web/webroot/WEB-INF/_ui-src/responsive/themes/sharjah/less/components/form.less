.form {
	padding: 40px 48px;
	@media @mquery-desktop {
		padding: 35px 0;
	}
	&-control {
		height: inherit;
		select& {
			-moz-appearance: none;
			-webkit-appearance: none;
			background: url('@{img-theme-path}svg/arrow-down-carnation.svg') no-repeat 95% center;
			background-size: auto 80%;
			border-radius: 6px;
			display: inline-block;
			font-family: 'roboto-light', arial, sans-serif;
			font-size: 32px;
			height: 100%;
			padding: 36px 100px 36px 36px;
			text-transform: inherit;
			@media @mquery-desktop {
				background: url('@{img-theme-path}svg/arrow-down-carnation.svg') no-repeat 97% center;
				background-size: auto 30px;
				border-radius: 3px;
				font-size: 16px;
				padding: 18px 50px 18px 18px;
			}
			&.active {
				background: transparent;
			}
			option {
				color: black;
			}
		}
	}
	&-label {
		color: @text-color;
		font-family: 'roboto-bold', arial, sans-serif;
		font-size: 32px;
		padding: 0 0 15px 0;
		text-transform: inherit;
		@media @mquery-desktop {
			font-size: 16px;
			padding: 0 0 7px 0;
		}
		> a {
			color: @color-secondary;
			display: inline-block;
			font-family: 'roboto-regular', arial, sans-serif;
			font-size: 32px;
			@media @mquery-desktop {
				font-size: 16px;
			}
			@media (max-width: @screen-sm-max) {
				margin-bottom: 60px;
			}
		}
	}
	.checkbox {
		text-transform: inherit;
		&--text {
			color: @color-bombay;
			display: block;
			font-family: 'roboto-light', arial, sans-serif;
			font-size: 40px;
			min-height: 96px;
			padding: 20px 0 0 127px;
			position: relative;
			text-transform: inherit;
			@media @mquery-desktop {
				font-size: 20px;
				min-height: 48px;
				padding: 10px 0 0 64px;
			}
			&:before {
				background-color: @color-white;
				border: 1px solid @color-mercury;
				border-radius: 5px;
				content: '';
				display: block;
				height: 96px;
				left: 0;
				position: absolute;
				top: 0;
				width: 96px;
				@media @mquery-desktop {
					border-radius: 3px;
					height: 48px;
					width: 48px;
				}
			}
			a {
				color: @color-carnation;
			}
		}
		&--wrapper {
			margin: 0 0 2rem;
			>div:nth-child(n+2) {
				margin: 2rem 0 0;
			}
		}
		.control-label {
			margin: 0;
		}
	}
	.form-group {
		margin: 0 0 26px;
		@media @mquery-desktop {
			margin: 0 0 13px;
		}
		.control-label {
			color: @text-color;
			font-family: 'roboto-bold', arial, sans-serif;
			font-size: 32px;
			padding: 0 0 15px 0;
			text-transform: inherit;
			@media @mquery-desktop {
				font-size: 16px;
				padding: 0 0 7px 0;
			}
		}
	}
	.button {
		font-size: 40px;
		width: 100%;
		@media @mquery-desktop {
			font-size: 20px;
		}
	}
	input {
		&[type='checkbox'] {
			display: none;
			&:checked {
				&~.checkbox--text {
					&:before {
						background: url('@{img-theme-path}svg/check.svg') no-repeat center center;
						background-size: 75% auto;
					}
				}
			}
		}
		&[type='email'],
		&[type='number'],
		&[type='password'],
		&[type='text'] {
			border-radius: 6px;
			font-family: 'roboto-regular', arial, sans-serif;
			font-size: 32px;
			padding: 32px;
			@media @mquery-desktop {
				border-radius: 3px;
				font-size: 16px;
				padding: 16px;
			}
			@media (max-width: @screen-sm-max) {
				padding-left: 16px;
				padding-right: 16px;
			}
		}
		&[type='number'] {
			-moz-appearance: textfield;
			&::-webkit-inner-spin-button,
			&::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
		}
		&[type='radio'],
		&[type='checkbox'] {
			margin-bottom: 0;
		}
		&[type='radio'] {
			display: none;
			&:checked+label {
				&:after {
					background: @text-color;
					background: url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" version="1.1" preserveAspectRatio="none" height="100" width="100"><circle cx="50" cy="50" r="40" stroke="transparent" stroke-width="0" fill="black" /></svg>') no-repeat center center;
					background-size: 100% auto;
					border-radius: 50%;
					content: '';
					height: 25px;
					left: 12px;
					position: absolute;
					top: 50%;
					transform: translateY(-50%);
					width: 25px;
					@media @mquery-desktop {
						left: 6px;
						height: 12px;
						width: 12px;
					}
				}
			}
			&+label {
				color: @color-dark-grey;
				font-family: 'roboto-light', arial, sans-serif;
				font-size: 32px;
				font-weight: inherit;
				padding: 0 0 0 80px;
				position: relative;
				text-transform: inherit;
				@media @mquery-desktop {
					font-size: 16px;
					padding: 0 0 0 40px;
				}
				&:before {
					background-color: @color-white;
					border: 1px solid @color-mercury;
					border-radius: 50%;
					content: '';
					height: 48px;
					left: 0;
					position: absolute;
					top: 50%;
					transform: translateY(-50%);
					width: 48px;
					@media @mquery-desktop {
						height: 24px;
						width: 24px;
					}
				}
			}
		}
	}
	p {
		font-family: 'roboto-light', arial, sans-serif;
		font-size: 32px;
		text-transform: inherit;
		@media @mquery-desktop {
			font-size: 16px;
		}
	}
}
.product-facet input[type='checkbox'] {
    margin-bottom: 0;
    margin-right: 0.3846153846153847rem;
}
