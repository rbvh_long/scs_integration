<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>

<c:url value="/cart/add" var="addToCartUrl"/>
<c:url value="${product.url}/configuratorPage/${configuratorType}" var="configureProductUrl"/>

<product:addToCartTitle/>

<form:form method="post" id="configureForm" class="configure_form" action="${configureProductUrl}">
	<c:if test="${product.purchasable}">
		<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
	</c:if>
	<input type="hidden" name="productCodePost" value="${product.code}"/>
	
	<c:if test="${empty showAddToCart ? true : showAddToCart}">
		<c:set var="buttonType">button</c:set>
		<c:if test="${product.stock.stockLevelStatus.code ne 'outOfStock' }">
			<c:set var="buttonType">submit</c:set>
		</c:if>
		<c:choose>
			<c:when test="${fn:contains(buttonType, 'button')}">
				<c:if test="${product.configurable}">
					<button id="configureProduct" type="button" class="add-button-product-toCart btn btn-block js-enable-btn outOfStock" disabled="disabled">
						<spring:theme code="basket.configure.product"/>
					</button>
				</c:if>
			</c:when>
			<c:otherwise>
            <c:if test="${product.configurable}">
            	<button id="configureProduct" type="${buttonType}" class="btn btn-block js-enable-btn" disabled="disabled"
                        name="configure">
                	<spring:theme code="basket.configure.product"/>
            	</button>
            </c:if>
			</c:otherwise>
		</c:choose>
	</c:if>
</form:form>
<form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">
	<c:if test="${product.purchasable}">
		<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1">
	</c:if>
	<input type="hidden" name="productCodePost" value="${product.code}"/>
	<input type="hidden" name="productConfOption" value=""/>

	<c:if test="${empty showAddToCart ? true : showAddToCart}">
		<c:set var="buttonType">button</c:set>
		<c:if test="${product.stock.stockLevelStatus.code ne 'outOfStock' }">
			<c:set var="buttonType">submit</c:set>
		</c:if>
		<c:choose>
			<c:when test="${fn:contains(buttonType, 'button')}">
				<div class="add-button-product">
			      <button class="add-button-product-whishlist">
			        	<addonProduct:productAddToWishlistButton product="${product}" />
			      </button>
				</div>
			</c:when>
			<c:otherwise>
				<div class="add-button-product">
					<ycommerce:testId code="addToCartButton">
						<button id="addToCartButton" type="button" class="add-button-product-toCart js-enable-btn">
							<span><spring:theme code="text.addToCart" /></span>
						</button>
					</ycommerce:testId>
					<button class="add-button-product-whishlist">
						<addonProduct:productAddToWishlistButton product="${product}" />
					</button>
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
</form:form>

