<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<div class="wishListPopin">		
		
	<h2><spring:theme code="kacc.wishlist.add.wishlist.message"/> </h2>
	<div class="inputStyel">				 
		<input type="text" placeholder="Nom de la liste" class="required" name="idNewWishName" id="idNewWishName">					 
		<input type="submit" value="<spring:theme code="kacc.wishlist.submit.wishlist.message" />" id="idCreateWishlist" onclick="performCreateWishlistFromAccount()">
		
		 
		<div class="errorValidationEmpty">
			<spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/>
		</div>
		<div class="errorValidationUnique">
			<spring:theme code="kacc.wishlist.account.add.newWishlist.errorUnique"/>
		</div>
		<div class="errorSizeMessage">
			<spring:theme code="kacc.wishlist.account.add.newWishlist.maxSizeTen"/>
		</div>
		<div class="errorMaxCharacter">
			<spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/>
		</div>
	</div>
</div>
