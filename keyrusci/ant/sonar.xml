<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project name="sonar" xmlns:sonar="antlib:org.sonar.ant">
	<import file="./common.xml" />

	<target name="kci-unittests" description="KeyrusCI - Launch unittests within a coverage inspector followed by a sonar analysis.">
		<coverage anttarget="unittests" />
	</target>

	<target name="kci-fastunittests">
		<nosonar anttarget="unittests" />
	</target>

	<target name="kci-alltests" description="KeyrusCI - Launch alltests within a coverage inspector followed by a sonar analysis.">
		<coverage anttarget="alltests" />
	</target>

	<macrodef name="coverage">
		<attribute name="anttarget" />
		<sequential>
			<taskdef uri="antlib:org.sonar.ant" resource="org/sonar/ant/antlib.xml">
				<classpath>
					<path path="${ext.keyrusci.path}/ant/lib/sonarqube-ant-task-2.5.jar" />
				</classpath>
			</taskdef>

			<load-project-version />
			<property name="sonar.projectVersion" value="${project.version}" />

			<prop-require prop="sonar.host.url" defaultValue="https://sonar.keyrus.info" />
			<prop-require prop="sonar.projectKey" defaultValue="superprojectkey" />
			<prop-require prop="sonar.projectName" defaultValue="Superproject" />
			<prop-require prop="custom.extensions" defaultValue="" />
			<echo level="info" message="--------------------------------" />
			<echo level="info" message="Calculating tests coverage" />
			<echo level="info" />
			<echo level="info" message="- Jacoco report path      => ${sonar.jacoco.reportPath}" />
			<echo level="info" message="- Jacoco agent jar path   => ${jacoco.agent.jar.path}" />
			<echo level="info" message="- Standalone java options => ${standalone.javaoptions}" />
			<echo level="info" message="- Junit reports path      => ${sonar.junit.reportsPath}" />
			<echo level="info" />
			<echo level="info" message="--------------------------------" />

			<delete file="${sonar.jacoco.reportPath}" failonerror="false" />
			<echo message="Lauching target @{anttarget} with java=${sonar.javaoptions}" />
			<antcall target="@{anttarget}">
				<param name="testclasses.packages" value="${analyse.package.restriction}" />
				<param name="standalone.javaoptions" value="${sonar.javaoptions}" />
			</antcall>
			<sonar extensions="${custom.extensions}" />
		</sequential>
	</macrodef>

	<macrodef name="sonar">
		<attribute name="extensions" />
		<sequential>
			<property name="sonar.excludedExtensions" value="" />

			<propertyregex property="sonar.extensions.clean" input="@{extensions}" regexp=";" replace="," global="true" defaultValue="@{extensions}" />
			<propertyregex property="sonar.excludedExtensions.clean" input="${sonar.excludedExtensions}" regexp=";" replace="," global="true" defaultValue="${sonar.excludedExtensions}" />

			<var name="isexcluded" value="" />
			<var name="affectedextensions" value="" />

			<for list="${sonar.extensions.clean}" param="extname" delimiter=",">
				<sequential>
					<for list="${sonar.excludedExtensions.clean}" param="excludedextensions" delimiter=",">
						<sequential>
							<if>
								<equals arg1="@{extname}" arg2="@{excludedextensions}" />
								<then>
									<var name="isexcluded" value="true" />
								</then>
							</if>
						</sequential>
					</for>
					<if>
						<equals arg1="${isexcluded}" arg2="true" />

						<then>
							<var name="isexcluded" value="false" />
						</then>
						<else>
							<if>
								<equals arg1="${affectedextensions}" arg2="" />
								<then>
									<var name="affectedextensions" value="@{extname}" />
								</then>
								<else>
									<var name="affectedextensions" value="${affectedextensions},@{extname}" />
								</else>
							</if>
						</else>
					</if>
				</sequential>
			</for>

			<sonarmulti modules="${affectedextensions}" />
			<sonar:sonar />

		</sequential>
	</macrodef>

	<macrodef name="sonarmulti">
		<attribute name="modules" />
		<sequential>
			<property name="sonar.extensions" value="@{modules}" />
			<property name="sonar.sourceEncoding" value="UTF-8" />
			<property name="sonar.projectName" value="Superproject" />
			<property name="sonar.projectKey" value="superprojectkey" />
			<property name="sonar.projectVersion" value="1.0" />

			<echo level="info" message=" " />
			<echo level="info" message="*************************************************" />
			<echo level="info" message="- Executing sonar" />
			<echo level="info" message="-------------------------------------------------" />
			<echo level="info" message="- sonar.extensions         : ${sonar.extensions}" />
			<echo level="info" message="- sonar.host.url           : ${sonar.host.url}" />
			<echo level="info" message="- sonar.projectName        : ${sonar.projectName}" />
			<echo level="info" message="- sonar.project.key        : ${sonar.projectKey}" />
			<echo level="info" message="- sonar.project.version    : ${sonar.projectVersion}" />
			<echo level="info" message="- sonar.excludedExtensions : ${sonar.excludedExtensions.clean}" />
			<echo level="info" message="*************************************************" />
			<echo level="info" message=" " />

			<var name="modulerelative" value="" />

			<for list="@{modules}" param="extname" delimiter=",">
				<sequential>
					<!-- Only modules with src folder -->
					<if>
						<or>
							<available file="${ext.@{extname}.path}/src" type="dir" />
							<available file="${ext.@{extname}.path}/web/src" type="dir" />
						</or>
						<then>
							<!-- generate modules for sonar -->
							<property name="@{extname}.relative" value="${ext.@{extname}.path}" relative="true" />
							<var name="modulerelative" value="${modulerelative}${@{extname}.relative}," />

							<!-- settings for each module -->
							<property name="${@{extname}.relative}.sonar.projectName" value="@{extname}" />
							<property name="${@{extname}.relative}.sonar.projectKey" value="@{extname}" />
							<property name="${@{extname}.relative}.sonar.projectVersion" value="1.0" />

							<!-- set src for each module folder -->
							<var name="temp.src" value="" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="src" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="web/src" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="gensrc" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="web/gensrc" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="resources" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="web/webroot" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="acceleratoraddon/web/src" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="acceleratoraddon/web/webroot" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="commonweb/src" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="backoffice/src" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="backoffice/resources" />
							<add-existing-folder prop="temp.src" parent="${ext.@{extname}.path}" folder="hmc/src" />
							<property name="${@{extname}.relative}.sonar.sources" value="${temp.src}" />

							<!-- test source -->
							<var name="temp.test.src" value="" />
							<add-existing-folder prop="temp.test.src" parent="${ext.@{extname}.path}" folder="testsrc" />
							<add-existing-folder prop="temp.test.src" parent="${ext.@{extname}.path}" folder="web/testsrc" />
							<add-existing-folder prop="temp.test.src" parent="${ext.@{extname}.path}" folder="backoffice/testsrc" />
							<property name="${@{extname}.relative}.sonar.tests" value="${temp.test.src}" />

							<!-- set classes folder for required extensions -->
							<var name="requiredcp" value="" />
							<for list="${all.required.extensions.for.@{extname}}" param="reqextname" delimiter=",">
								<sequential>
									<add-existing-folder prop="requiredcp" parent="${ext.@{reqextname}.path}" folder="classes" absolute="true" />
								</sequential>
							</for>

							<path id='sonar.libraries.classpath'>
								<fileset erroronmissingdir="false" dir="${ext.@{extname}.path}/lib">
									<include name="**/*.jar" />
								</fileset>
								<fileset dir="${platformhome}/ext/core/lib">
									<include name="**/*.jar" />
								</fileset>
								<fileset erroronmissingdir="false" dir="${ext.@{extname}.path}/web/webroot/WEB-INF/lib">
									<include name="**/*.jar" />
								</fileset>
								<fileset dir="${platformhome}/lib/">
									<include name="**/*.jar" />
								</fileset>
								<fileset dir="${platformhome}/bootstrap/bin/">
									<include name="**/*.jar" />
								</fileset>
								<fileset dir="${bundled.tomcat.home}/lib/">
									<include name="**/*.jar" />
								</fileset>
								<fileset dir="${platformhome}/resources/configtemplates/develop/licence/">
									<include name="**/*.jar" />
								</fileset>
							</path>

							<!-- get all jars from required extensions -->
							<var name="reqextname.lib.path" value="" />
							<for list="${all.required.extensions.for.@{extname}}" param="reqextname" delimiter=",">
								<sequential>
									<path id='@{reqextname}.libraries.classpath'>
										<fileset erroronmissingdir="false" dir="${ext.@{reqextname}.path}/bin">
											<include name="**/*.jar" />
										</fileset>
										<fileset erroronmissingdir="false" dir="${ext.@{reqextname}.path}/lib">
											<include name="**/*.jar" />
										</fileset>
										<fileset erroronmissingdir="false" dir="${ext.@{reqextname}.path}/web/webroot/WEB-INF/lib">
											<include name="**/*.jar" />
										</fileset>
									</path>
									<pathconvert property="@{reqextname}.libraries.cp" refid="@{reqextname}.libraries.classpath" pathsep="," />
									<var name="reqextname.lib.path" value="${reqextname.lib.path},${@{reqextname}.libraries.cp}" />
								</sequential>
							</for>

							<!-- Standard classpath -->
							<pathconvert property="standart.@{extname}.cp" refid="sonar.libraries.classpath" pathsep="," />

							<!-- libraries from required extensions classpath -->
							<property name="${@{extname}.relative}.sonar.java.libraries" value="${standart.@{extname}.cp},${reqextname.lib.path}" />
							<property name="${@{extname}.relative}.sonar.java.test.libraries" value="${standart.@{extname}.cp},${reqextname.lib.path}" />

							<!-- put binaries from this extension and required extensions togetter and set property -->
							<var name="binaries.path" value="" />
							<add-existing-folder prop="binaries.path" parent="${ext.@{extname}.path}" folder="classes" />
							<add-existing-folder prop="binaries.path" parent="${ext.@{extname}.path}" folder="web/webroot/WEB-INF/classes" />
							<add-existing-folder prop="binaries.path" parent="${ext.@{extname}.path}" folder="backoffice/classes" />
							<add-existing-folder prop="binaries.path" parent="${ext.@{extname}.path}" folder="hmc/classes" />
							<property name="${@{extname}.relative}.sonar.java.binaries" value="${binaries.path},${requiredcp}" />
							<property name="${@{extname}.relative}.sonar.java.test.binaries" value="${binaries.path},${requiredcp}" />
						</then>
					</if>
				</sequential>
			</for>

			<property name="sonar.modules" value="${modulerelative}" />

		</sequential>
	</macrodef>

	<macrodef name="add-existing-folder">
		<attribute name="prop" />
		<attribute name="parent" />
		<attribute name="folder" />
		<attribute name="absolute" default="false" />
		<sequential>
			<var name="pathtoadd" value="@{folder}" />
			<if>
				<istrue value="@{absolute}" />
				<then>
					<var name="pathtoadd" value="@{parent}/@{folder}" />
				</then>
			</if>
			<if>
				<available file="@{parent}/@{folder}" type="dir" />
				<then>
					<if>
						<not>
							<equals arg1="${@{prop}}" arg2="" />
						</not>
						<then>
							<var name="@{prop}" value="${@{prop}}," />
						</then>
					</if>
					<var name="@{prop}" value="${@{prop}}${pathtoadd}" />
				</then>
			</if>
		</sequential>
	</macrodef>

	<macrodef name="nosonar">
		<attribute name="anttarget" />
		<sequential>
			<antcall target="@{anttarget}">
				<param name="testclasses.packages" value="${analyse.package.restriction}" />
			</antcall>
		</sequential>
	</macrodef>

</project>